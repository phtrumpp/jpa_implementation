package de.academy.jpa.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Comment {


    @Id
    @GeneratedValue
    private Long id;

    private String inhalt;

    // Das hier steht versteckt, n - 1 Beziehungen müssen nicht deklariert werden - Maher erklären
//    @ManyToOne
//    private Post post;



    //default Konstruktor
    public Comment() {
    }
    public Comment(String inhalt) {
        this.inhalt = inhalt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInhalt() {
        return inhalt;
    }

    public void setInhalt(String inhalt) {
        this.inhalt = inhalt;
    }
}
