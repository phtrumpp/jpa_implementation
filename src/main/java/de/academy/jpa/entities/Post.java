package de.academy.jpa.entities;


import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Post  {



    // Konstruktoren - wichtig: Entities benötigen immer einen default Konstruktor
    public Post() {
    }
    public Post(String title, int age) {
        this.title = title;
        this.age = age;
    }

    @Id
    @GeneratedValue
    private Long id;

    //private List<Comment> comments;

    @OneToMany
    @JoinColumn
    private List<Comment> comments = new ArrayList<>(); // 1 zu n oder generell Beziehungen von einer Entität zu einer anderen
    // müssen immer mit einer entsprechenden Annotation deklariert werden

    private String name;

    private int age;

    //@Column(name="wohnort")
    private String adresse;

    @Transient
    private float unsinn;

    @Column(nullable = false)
    private String title;

    //private LocalDateTime serverTime;



    /**
     * Getter und Setter
     * @return
     */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int alter) {
        this.age = alter;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public float getUnsinn() {
        return unsinn;
    }

    public void setUnsinn(float unsinn) {
        this.unsinn = unsinn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Comment> getComments() {
        return comments;
    }

    //    public LocalDateTime getServerTime() {
//        return serverTime;
//    }
//
//    public void setServerTime(LocalDateTime serverTime) {
//        this.serverTime = serverTime;
//    }
}
