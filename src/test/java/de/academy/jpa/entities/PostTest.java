package de.academy.jpa.entities;


import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import java.util.List;

import static org.junit.Assert.assertTrue;


public class PostTest {


    private static EntityManager entityManager;


    // 1. Legen  (@Before) eine init Methode fest vor, die den EntityManager erzeugt. Noch bevor junit einzelne Tests startet
    @Before
    public void init() {

        EntityManagerFactory entityManagerFactory
                = Persistence.createEntityManagerFactory("JPA_WEBBLOG_PERSISTENCE_UNIT");

        entityManager = entityManagerFactory.createEntityManager();

    }

    @Test
    public void testMethode1(){

        //Test data  preparation
        Post readPost= new  Post("Buchtitel", 12);
        entityManager.getTransaction().begin();
        entityManager.persist(readPost);
        entityManager.getTransaction().commit();
        Long id = readPost.getId();

        //Prüfung ob in DatenBank gespeichert ist
        Post post= entityManager.find(Post.class,  id);
        assertTrue(post.getId() == id);
    }

//    @Test
//    public void commentTest(){
//
//        Comment newComment = new Comment("Kommentar-Inhalt");
//        entityManager.getTransaction().begin();
//        entityManager.persist(newComment);
//        entityManager.getTransaction().commit();
//
//        // Prüfung ob gespeichert in Datenbank
//        entityManager.find(Comment.class,newComment.getInhalt());
//        //assertTrue(newComment.getInhalt()==);
//    }

    @Test
    public void commentToPost(){

        Post p = new Post("lol",12);
        entityManager.getTransaction().begin();
        entityManager.persist(p);

        Comment c = new Comment();
        entityManager.persist(c);

        p.getComments().add(c);
        entityManager.persist(c);
        entityManager.getTransaction().commit();

        // Brauch ich die hier?
        Long commentID = c.getId();
        Long postID = p.getId();

        // Prüfung ob gespeichertes Element übereinstimmt
        Post postWithComment = entityManager.find(Post.class,p.getId());
        Comment commentInPost = entityManager.find(Comment.class, c.getId());

        assertTrue(postWithComment.getComments().get(0).getId().longValue() == commentInPost.getId().longValue());
    }



}